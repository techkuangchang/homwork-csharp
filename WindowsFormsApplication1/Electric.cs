﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Electric
    {

        public double powerused(double newNum, double oldNum)
        {
            return newNum - oldNum;
        }

        public double payments(double powerUsed)
        {
            double result = 0;

            if (powerUsed >= 0 && powerUsed < 200)
            {
                result = powerUsed * 610;
            }
            else if (powerUsed >= 50 && powerUsed < 200)
            {
                result = powerUsed * 710;
            }
            else if (powerUsed >= 200)
            {
                result = powerUsed * 780;
            }
            return result;
        }
    }
}
