﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Electricity : Form
    {
        double newNum, oldNum, result, payment;
        Electric electricity = new Electric();


        public Electricity()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtPayment_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(txtNewNum.Text) || String.IsNullOrEmpty(txtOldNum.Text))
            {
                MessageBox.Show("Please input value", "warning");
                clears();
            }
            else
            {
                newNum = Convert.ToDouble(txtNewNum.Text);
                oldNum = Convert.ToDouble(txtOldNum.Text);
                if(oldNum > newNum)
                {
                    MessageBox.Show("Invalid input data", "warning");
                    clears();
                }
                else
                {
                    result = electricity.powerused(newNum, oldNum);
                    payment = electricity.payments(result);
                    txtPayment.Text = payment.ToString();
                    txtPower.Text = result.ToString();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            clears();
        }

        private void clears()
        {
            txtNewNum.Text = "";
            txtOldNum.Text = "";
            txtPayment.Text = "";
            txtPower.Text = "";
            txtNewNum.Focus();
        }
    }
}
