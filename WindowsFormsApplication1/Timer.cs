﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Timer : Form
    {
        public Timer()
        {
            InitializeComponent();
        }

        private void Timer_Load(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Now.ToString("dd:MM:yyyy");
            lblDay.Text = DateTime.Now.ToString("dddd");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblHour.Text = DateTime.Now.ToString("hh:mm");
            lblMinute.Text = DateTime.Now.ToString("ss");
            timer1.Enabled = true;
            timer1.Start();
        }
    }
}
